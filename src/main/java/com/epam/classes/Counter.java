package src.main.java.com.epam.classes;

public class Counter {
    private int thresholdFrom = 0;
    private int thresholdTo = 1;
    private int current;

    public Counter() {
        this.thresholdFrom = 0;
        this.thresholdTo = 5;
        this.current = 0;
    }

    public Counter(int thresholdFrom, int thresholdTo) {
        if (thresholdFrom > thresholdTo) {
            System.out.println("Instance can't be created");
            return;
        }
        this.thresholdFrom = thresholdFrom;
        this.thresholdTo = thresholdTo;
        this.current = thresholdFrom;
    }

    public int increase() {
        if (current == thresholdTo) {
            current = thresholdFrom;
            return current;
        }

        return ++current;
    }

    public int decrease() {
        if (current == thresholdFrom) {
            current = thresholdTo;
            return current;
        }

        return --current;
    }

    public int getThresholdFrom() {
        return thresholdFrom;
    }

    public void setThresholdFrom(int thresholdFrom) {
        if (thresholdFrom > thresholdTo || thresholdFrom > current) {
            System.out.println("Can't set value more than thresholdTo or more than current value");
        } else {
            this.thresholdFrom = thresholdFrom;
        }
    }

    public int getThresholdTo() {
        return thresholdTo;
    }

    public void setThresholdTo(int thresholdTo) {
        if (thresholdTo < thresholdFrom || thresholdTo < current) {
            System.out.println("Can't set value less than thresholdFrom or less than current value");
        } else {
            this.thresholdTo = thresholdTo;
        }
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        if (current < this.thresholdFrom || current > this.thresholdTo) {
            System.out.println("Can't set value more than thresholdTo or less than thresholdFrom");
        } else {
            this.current = current;
        }
    }

    @Override
    public String toString() {
        return "Counter{" +
                "thresholdFrom=" + thresholdFrom +
                ", thresholdTo=" + thresholdTo +
                ", counter=" + current +
                '}';
    }
}

package src.main.java.com.epam.classes;


public class Main {

    public static void main(String[] args) {
        testTest1();
        testTest2();
        testTriangle();
        testCounter();
        testTime();
    }

    public static void testTest1() {
        System.out.println("-- testing class Test1 --");
        Test1 test1 = new Test1(1, 5);

        System.out.println(test1.getSum());
        System.out.println(test1.getLargest());
        test1.setValue1(3);
        test1.setValue2(9);
        System.out.println(test1.getSum());
        System.out.println(test1.getLargest());
        System.out.println(test1.toString());

        System.out.println("-- end testing class Test1 --");
        System.out.printf("\n\n\n");
    }

    public static void testTest2() {
        System.out.println("-- testing class Test2 --");
        Test2 test2 = new Test2(1, 2);

        System.out.println("value1 = " + test2.getValue1() + " value2 = " + test2.getValue2());
        test2.setValue1(6);
        test2.setValue2(9);
        System.out.println("value1 = " + test2.getValue1() + " value2 = " + test2.getValue2());

        System.out.println("-- end testing class Test2 --");
        System.out.printf("\n\n\n");
    }

    public static void testTriangle() {
        System.out.println("-- testing class Triangle --");
        Triangle triangle = new Triangle(4, 3, 45);

        System.out.println(triangle.getSideC());
        System.out.println(triangle.getArea());
        System.out.println(triangle.getPerimeter());

        System.out.println("-- end testing class Triangle --");
        System.out.printf("\n\n\n");
    }

    public static void testCounter() {
        System.out.println("-- testing class Counter --");
        int thresholdFrom = 0;
        int thresholdTo = 10;
        Counter counter = new Counter(thresholdFrom, thresholdTo);

        System.out.println("-- counting from 0 to 10 --");
        for (int i = thresholdFrom; i < thresholdTo; i++) {
            System.out.print(counter.increase() + " ");
        }
        System.out.println();
        System.out.println("-- counting forward when counter value equals 10 --");
        System.out.println(counter.increase());
        System.out.println("-- counting back when counter value equals 0 --");
        System.out.println(counter.decrease());

        System.out.println("-- trying to set invalid values --");
        counter.setThresholdFrom(11);
        counter.setThresholdTo(-1);
        counter.setCurrent(-1);

        System.out.println("-- setting threshold values and counting with new numbers");
        counter.setThresholdFrom(-1);
        counter.setThresholdTo(11);
        counter.setCurrent(-1);

        System.out.println("-- counting from -1 to 11 --");
        System.out.println(counter.getCurrent());
        for (int i = counter.getThresholdFrom(); i < counter.getThresholdTo(); i++) {
            System.out.print(counter.increase() + " ");
        }
        System.out.println();

        System.out.println("-- end testing class Counter --");
        System.out.printf("\n\n\n");
    }

    public static void testTime() {
        System.out.println("-- testing class Time --");
        Time time = new Time();

        System.out.println("-- setting invalid fields");
        time.setHours(-1);
        time.setMinutes(-1);
        time.setSeconds(-1);

        System.out.println("-- setting valid fields");
        time.setHours(14);
        time.setMinutes(12);
        time.setSeconds(35);

        String humanReadableTime = time.getHumanReadableTime();
        System.out.println(humanReadableTime);

        System.out.println("-- trying to create instance with invalid fields");
        Time time2 = new Time(-3, 60, 91);

        System.out.println("-- end testing class Time --");
        System.out.printf("\n\n\n");
    }
}

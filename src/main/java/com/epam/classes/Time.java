package src.main.java.com.epam.classes;

public class Time {
    private int hours;
    private int minutes;
    private int seconds;

    public Time() {
        this.hours = 0;
        this.minutes = 0;
        this.seconds = 0;
    }

    public Time(int hours, int minutes, int seconds) {
        if (hours > 23 || hours < 0) {
            System.out.println("Instance can't be created: invalid hours");
        } else if (minutes > 59 || minutes < 0) {
            System.out.println("Instance can't be created: invalid minutes");
        } else if (seconds > 59 || seconds < 0) {
            System.out.println("Instance can't be created: invalid seconds");
        } else {
            this.hours = hours;
            this.minutes = minutes;
            this.seconds = seconds;
        }
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        if (hours > 23 || hours < 0) {
            System.out.println("Field can't be filled: invalid hours");
        } else {
            this.hours = hours;
        }
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        if (minutes > 59 || minutes < 0) {
            System.out.println("Field can't be filled: invalid minutes");
        } else {
            this.minutes = minutes;
        }
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        if (seconds > 59 || seconds < 0) {
            System.out.println("Field can't be filled: invalid seconds");
        } else {
            this.seconds = seconds;
        }
    }

    @Override
    public String toString() {
        return "Time{" +
                "hours=" + hours +
                ", minutes=" + minutes +
                ", seconds=" + seconds +
                '}';
    }

    public String getHumanReadableTime() {
        String humanReadableTime = String.format("%d:%d:%d", hours, minutes, seconds);

        return humanReadableTime;
    }
}

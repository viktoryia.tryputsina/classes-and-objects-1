package src.main.java.com.epam.classes;

public class Triangle {

    // as we agreed on our seminar lesson, we describe Triangle object using or three points, or two sides and an angle
    private double sideA;
    private double sideB;
    private double sideC;
    private double angle;

    public Triangle(double lengthSide1, double lengthSide2, double angle) {
        this.sideA = lengthSide1;
        this.sideB = lengthSide2;
        this.sideC = Math.sqrt(
                Math.pow(lengthSide1, 2) +
                        Math.pow(lengthSide2, 2) -
                        (lengthSide1 * lengthSide2 * Math.cos(this.getRadiansOfAngle(angle)) * 2.0)
        );
        this.angle = angle;
    }

    public double getArea() {
        double area = sideA * sideB * Math.sin(this.getRadiansOfAngle(angle)) / 2;
        return area;
    }

    public double getPerimeter() {
        double perimeter = sideA + sideB + sideC;
        return perimeter;
    }

    public double getSideC() {
        return sideC;
    }

    public double getRadiansOfAngle(double angle) {
        return (Math.PI * angle) / 180;
    }

    public double getSideA() {
        return sideA;
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public void setSideB(double sideB) {
        this.sideB = sideB;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Triangle triangle = (Triangle) o;

        if (Double.compare(triangle.sideA, sideA) != 0) {
            return false;
        }
        if (Double.compare(triangle.sideB, sideB) != 0) {
            return false;
        }
        if (Double.compare(triangle.sideC, sideC) != 0) {
            return false;
        }
        return Double.compare(triangle.angle, angle) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(sideA);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(sideB);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(sideC);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(angle);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "side1=" + sideA +
                ", side2=" + sideB +
                ", angle=" + angle +
                '}';
    }
}
